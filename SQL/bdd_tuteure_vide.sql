-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 04 fév. 2020 à 17:57
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_tuteure`
--

-- --------------------------------------------------------

--
-- Structure de la table `iota_complement_enseignant`
--

CREATE TABLE `iota_complement_enseignant` (
  `ens_comp_id` int(11) NOT NULL,
  `ens_USER_id` int(11) NOT NULL,
  `ens_pro_link` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `ens_lab` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_exo`
--

CREATE TABLE `iota_exo` (
  `EXO_id` int(11) NOT NULL,
  `EXO_date_crea` date NOT NULL,
  `EXO_nom` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_content` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_USER_id` int(11) NOT NULL,
  `EXO_nb_eleve` int(11) NOT NULL,
  `EXO_lang` varchar(10) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_path` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_timeout` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_ref_exo_workspace`
--

CREATE TABLE `iota_ref_exo_workspace` (
  `ref_id` int(11) NOT NULL,
  `ref_EXO_id` int(11) NOT NULL,
  `ref_WORK_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_ref_user_exo`
--

CREATE TABLE `iota_ref_user_exo` (
  `ref_id` int(11) NOT NULL,
  `ref_USER_id` int(11) NOT NULL,
  `ref_EXO_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_ref_user_workspace`
--

CREATE TABLE `iota_ref_user_workspace` (
  `ref_id` int(11) NOT NULL,
  `ref_USER_id` int(11) NOT NULL,
  `ref_WORK_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_reponse`
--

CREATE TABLE `iota_reponse` (
  `REP_id` int(11) NOT NULL,
  `REP_USER_id` int(11) NOT NULL,
  `REP_EXO_id` int(11) NOT NULL,
  `REP_content` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_user`
--

CREATE TABLE `iota_user` (
  `USER_id` int(11) NOT NULL,
  `USER_nom` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `USER_prenom` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `USER_mail` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `USER_password` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `USER_datenais` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `USER_type` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Structure de la table `iota_workspace`
--

CREATE TABLE `iota_workspace` (
  `WORK_id` int(11) NOT NULL,
  `WORK_nom` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `WORK_desc` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `WORK_date` date NOT NULL,
  `WORK_USER_id` int(11) NOT NULL,
  `WORK_code` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `iota_complement_enseignant`
--
ALTER TABLE `iota_complement_enseignant`
  ADD PRIMARY KEY (`ens_comp_id`);

--
-- Index pour la table `iota_exo`
--
ALTER TABLE `iota_exo`
  ADD PRIMARY KEY (`EXO_id`);

--
-- Index pour la table `iota_ref_exo_workspace`
--
ALTER TABLE `iota_ref_exo_workspace`
  ADD PRIMARY KEY (`ref_id`);

--
-- Index pour la table `iota_ref_user_exo`
--
ALTER TABLE `iota_ref_user_exo`
  ADD PRIMARY KEY (`ref_id`);

--
-- Index pour la table `iota_ref_user_workspace`
--
ALTER TABLE `iota_ref_user_workspace`
  ADD PRIMARY KEY (`ref_id`);

--
-- Index pour la table `iota_reponse`
--
ALTER TABLE `iota_reponse`
  ADD PRIMARY KEY (`REP_id`);

--
-- Index pour la table `iota_user`
--
ALTER TABLE `iota_user`
  ADD PRIMARY KEY (`USER_id`);

--
-- Index pour la table `iota_workspace`
--
ALTER TABLE `iota_workspace`
  ADD PRIMARY KEY (`WORK_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `iota_complement_enseignant`
--
ALTER TABLE `iota_complement_enseignant`
  MODIFY `ens_comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `iota_exo`
--
ALTER TABLE `iota_exo`
  MODIFY `EXO_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT pour la table `iota_ref_exo_workspace`
--
ALTER TABLE `iota_ref_exo_workspace`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `iota_ref_user_exo`
--
ALTER TABLE `iota_ref_user_exo`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `iota_ref_user_workspace`
--
ALTER TABLE `iota_ref_user_workspace`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `iota_reponse`
--
ALTER TABLE `iota_reponse`
  MODIFY `REP_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `iota_user`
--
ALTER TABLE `iota_user`
  MODIFY `USER_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `iota_workspace`
--
ALTER TABLE `iota_workspace`
  MODIFY `WORK_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
