ALTER TABLE `iota_user` ADD `USER_datenais` VARCHAR(50) NOT NULL AFTER `USER_mail`;

ALTER TABLE `iota_user` ADD `USER_password` VARCHAR(255) NOT NULL AFTER `USER_mail`;

CREATE TABLE `bdd_tuteure`.`iota_complement_enseignant` ( `ens_comp_id` INT NOT NULL AUTO_INCREMENT , `ens_USER_id` INT NOT NULL , `ens_pro_link` VARCHAR(255) NOT NULL , `ens_lab` VARCHAR(255) NOT NULL , PRIMARY KEY (`ens_comp_id`)) ENGINE = InnoDB;

ALTER TABLE `iota_user` ADD `USER_type` VARCHAR(50) NOT NULL AFTER `USER_datenais`;