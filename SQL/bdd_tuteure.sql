-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 04 fév. 2020 à 20:21
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bdd_tuteure`
--

-- --------------------------------------------------------

--
-- Structure de la table `iota_complement_enseignant`
--

CREATE TABLE `iota_complement_enseignant` (
  `ens_comp_id` int(11) NOT NULL,
  `ens_USER_id` int(11) NOT NULL,
  `ens_pro_link` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `ens_lab` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_complement_enseignant`
--

INSERT INTO `iota_complement_enseignant` (`ens_comp_id`, `ens_USER_id`, `ens_pro_link`, `ens_lab`) VALUES
(1, 0, 'p', 'pp'),
(2, 18, 'p', 'pp'),
(3, 19, 'oo', 'foof'),
(4, 20, 'opo', 'opo'),
(5, 22, 'mm', 'mm'),
(6, 23, 'pp', 'pp'),
(7, 24, 'pp', 'pp'),
(8, 25, 'pp', 'pp'),
(9, 26, 'pp', 'pp'),
(10, 27, 'pp', 'pp'),
(11, 28, 'pp', 'pp'),
(12, 29, 'pp', 'pp'),
(13, 30, 'pp', 'pp'),
(14, 31, 'pp', 'pp'),
(15, 32, 'pp', 'pp'),
(16, 33, 'pp', 'pp'),
(17, 34, 'pp', 'pp'),
(18, 40, 'Lip6.fr', 'Lip6');

-- --------------------------------------------------------

--
-- Structure de la table `iota_exo`
--

CREATE TABLE `iota_exo` (
  `EXO_id` int(11) NOT NULL,
  `EXO_date_crea` date NOT NULL DEFAULT current_timestamp(),
  `EXO_nom` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_content` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_USER_id` int(11) DEFAULT NULL,
  `EXO_nb_eleve` int(11) DEFAULT NULL,
  `EXO_lang` varchar(10) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_path` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `EXO_timeout` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_exo`
--

INSERT INTO `iota_exo` (`EXO_id`, `EXO_date_crea`, `EXO_nom`, `EXO_content`, `EXO_USER_id`, `EXO_nb_eleve`, `EXO_lang`, `EXO_path`, `EXO_timeout`) VALUES
(11, '0000-00-00', '11pp', '11', 0, 0, '', '', 0),
(13, '0000-00-00', 'p', 'p', 0, 0, '', '', 0),
(19, '0000-00-00', 'mom', 'mom', 0, 0, '', '', 0),
(20, '0000-00-00', 'pat', 'pat', 0, 0, '', '', 0),
(21, '0000-00-00', 'app', 'app', 0, 0, '', '', 0),
(22, '0000-00-00', 'b', 'bbb', 0, 0, '', '', 0),
(23, '0000-00-00', 'bb', 'bb', 0, 0, '', '', 0),
(24, '0000-00-00', 'pp', 'pp', 0, 0, '', '', 0),
(25, '0000-00-00', 'square', 'float square(unsigned int i, signed int k, float s, char c) {\n    return i*i;\n{\n}{\n    // à remplir\n}', 0, 0, '', '', 0),
(26, '0000-00-00', 'square', 'float square(unsigned int i, signed int k, float s, char c) {\n    return i*i;\n{\n}{\n    // à remplir\n}', 0, 0, '', '', 0),
(27, '0000-00-00', 'square', 'float square(unsigned int i, signed int k, float s, char c) {\n    return i*i;\n{\n}{\n    // à remplir\n}', 0, 0, '', '', 0),
(29, '0000-00-00', 'square', 'float square(unsigned int i, signed int k, float s, char c) {\n    return i*i;\n{\n}{\n    // à remplir\n}', 0, 0, '', '', 0),
(30, '0000-00-00', 'square', 'float square(unsigned int i, signed int k, float s, char c) {\n// à remplir}', 0, 0, 'c', 'uploaded_code/square.c', 0),
(31, '0000-00-00', 'test', 'float test(unsigned int i, signed int k, float s, char c) {\n// à remplir', 0, 0, 'c', 'uploaded_code/test.c', 0),
(36, '0000-00-00', 'test', 'float test(unsigned int i, signed int k, float s, char c) {\n// à remplir', 0, 0, 'c', 'uploaded_code/test.c', 0),
(38, '0000-00-00', 'test', 'float test(unsigned int i, signed int k, float s, char c) {\n// à remplir', 0, 0, 'c', 'uploaded_code/test.c', 0),
(39, '0000-00-00', 'test', 'float test(unsigned int i, signed int k, float s, char c) {\n// à remplir', 0, 0, 'c', 'uploaded_code/test.c', 0),
(40, '0000-00-00', 'test', 'float test(unsigned int i, signed int k, float s, char c) {\n// à remplir', 0, 0, 'c', 'uploaded_code/test.c', 0),
(41, '0000-00-00', 'test', 'float test(unsigned int i, signed int k, float s, char c) {\n// à remplir', 0, 0, 'c', 'uploaded_code/test.c', 0),
(42, '0000-00-00', 'test2', 'float test2(unsigned int i, signed int k, float s, char c) {\n// à remplir\n}', 0, 0, 'c', 'uploaded_code/test2.c', 0),
(43, '2020-02-04', 'somme', 'int somme(int i, int k) {\n// à remplir\n}', NULL, NULL, 'c', 'uploaded_code/somme.c', 5),
(44, '2020-02-04', 'square', 'def square(i: int) -> int:\n  pass', NULL, NULL, 'py', 'uploaded_code/square.py', 5);

-- --------------------------------------------------------

--
-- Structure de la table `iota_ref_exo_workspace`
--

CREATE TABLE `iota_ref_exo_workspace` (
  `ref_id` int(11) NOT NULL,
  `ref_EXO_id` int(11) NOT NULL,
  `ref_WORK_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_ref_exo_workspace`
--

INSERT INTO `iota_ref_exo_workspace` (`ref_id`, `ref_EXO_id`, `ref_WORK_id`) VALUES
(1, 30, 4),
(3, 29, 4),
(4, 31, 4),
(5, 36, 4),
(6, 38, 4),
(7, 39, 4),
(8, 40, 4),
(9, 41, 4),
(10, 42, 5),
(11, 43, 7),
(12, 44, 7);

-- --------------------------------------------------------

--
-- Structure de la table `iota_ref_user_exo`
--

CREATE TABLE `iota_ref_user_exo` (
  `ref_id` int(11) NOT NULL,
  `ref_USER_id` int(11) NOT NULL,
  `ref_EXO_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_ref_user_exo`
--

INSERT INTO `iota_ref_user_exo` (`ref_id`, `ref_USER_id`, `ref_EXO_id`) VALUES
(1, 39, 30),
(2, 39, 30);

-- --------------------------------------------------------

--
-- Structure de la table `iota_ref_user_workspace`
--

CREATE TABLE `iota_ref_user_workspace` (
  `ref_id` int(11) NOT NULL,
  `ref_USER_id` int(11) NOT NULL,
  `ref_WORK_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_ref_user_workspace`
--

INSERT INTO `iota_ref_user_workspace` (`ref_id`, `ref_USER_id`, `ref_WORK_id`) VALUES
(1, 39, 5),
(2, 41, 7);

-- --------------------------------------------------------

--
-- Structure de la table `iota_reponse`
--

CREATE TABLE `iota_reponse` (
  `REP_id` int(11) NOT NULL,
  `REP_USER_id` int(11) NOT NULL,
  `REP_EXO_id` int(11) NOT NULL,
  `REP_content` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_reponse`
--

INSERT INTO `iota_reponse` (`REP_id`, `REP_USER_id`, `REP_EXO_id`, `REP_content`) VALUES
(1, 0, 0, '0'),
(2, 0, 0, '0'),
(3, 0, 0, 'patate'),
(4, 39, 30, 'float square(unsigned int i, signed int k, float s, char c) {\r\n// à remplir}'),
(5, 39, 30, 'float square(unsigned int i, signed int k, float s, char c) {\r\n// à remplir}');

-- --------------------------------------------------------

--
-- Structure de la table `iota_user`
--

CREATE TABLE `iota_user` (
  `USER_id` int(11) NOT NULL,
  `USER_nom` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `USER_prenom` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `USER_mail` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `USER_password` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `USER_datenais` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  `USER_type` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_user`
--

INSERT INTO `iota_user` (`USER_id`, `USER_nom`, `USER_prenom`, `USER_mail`, `USER_password`, `USER_datenais`, `USER_type`) VALUES
(19, 'foo', 'foo', 'phpierre@hotmail.fr', 'foo', '2020-02-20', 'enseignant'),
(39, 'pp', 'pp', 'bloopota@gmail.com', 'pp', '2020-02-03', 'eleve'),
(40, 'untel', 'untel', 'untel@edu.fr', 'aaaaa', '00-00-0000', 'enseignant'),
(41, 'rey', 'steve', 'aaaaa@gmail.com', 'aaaaa', '00-00-0000', 'eleve');

-- --------------------------------------------------------

--
-- Structure de la table `iota_workspace`
--

CREATE TABLE `iota_workspace` (
  `WORK_id` int(11) NOT NULL,
  `WORK_nom` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `WORK_desc` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `WORK_date` date NOT NULL DEFAULT current_timestamp(),
  `WORK_USER_id` int(11) NOT NULL,
  `WORK_code` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Déchargement des données de la table `iota_workspace`
--

INSERT INTO `iota_workspace` (`WORK_id`, `WORK_nom`, `WORK_desc`, `WORK_date`, `WORK_USER_id`, `WORK_code`) VALUES
(1, '', '', '2020-01-01', 1, 0),
(2, 'p', 'pop', '0000-00-00', 0, 0),
(3, 'poop', 'pop', '0000-00-00', 0, 0),
(4, 'lala', 'la', '0000-00-00', 0, 0),
(5, 'patate', 'patate', '0000-00-00', 0, 982018649),
(6, 'foo', 'foo', '0000-00-00', 0, 987028015),
(7, 'L3DANT', 'eodjeu', '2020-02-04', 40, 50034451);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `iota_complement_enseignant`
--
ALTER TABLE `iota_complement_enseignant`
  ADD PRIMARY KEY (`ens_comp_id`);

--
-- Index pour la table `iota_exo`
--
ALTER TABLE `iota_exo`
  ADD PRIMARY KEY (`EXO_id`);

--
-- Index pour la table `iota_ref_exo_workspace`
--
ALTER TABLE `iota_ref_exo_workspace`
  ADD PRIMARY KEY (`ref_id`);

--
-- Index pour la table `iota_ref_user_exo`
--
ALTER TABLE `iota_ref_user_exo`
  ADD PRIMARY KEY (`ref_id`);

--
-- Index pour la table `iota_ref_user_workspace`
--
ALTER TABLE `iota_ref_user_workspace`
  ADD PRIMARY KEY (`ref_id`);

--
-- Index pour la table `iota_reponse`
--
ALTER TABLE `iota_reponse`
  ADD PRIMARY KEY (`REP_id`);

--
-- Index pour la table `iota_user`
--
ALTER TABLE `iota_user`
  ADD PRIMARY KEY (`USER_id`);

--
-- Index pour la table `iota_workspace`
--
ALTER TABLE `iota_workspace`
  ADD PRIMARY KEY (`WORK_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `iota_complement_enseignant`
--
ALTER TABLE `iota_complement_enseignant`
  MODIFY `ens_comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `iota_exo`
--
ALTER TABLE `iota_exo`
  MODIFY `EXO_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT pour la table `iota_ref_exo_workspace`
--
ALTER TABLE `iota_ref_exo_workspace`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `iota_ref_user_exo`
--
ALTER TABLE `iota_ref_user_exo`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `iota_ref_user_workspace`
--
ALTER TABLE `iota_ref_user_workspace`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `iota_reponse`
--
ALTER TABLE `iota_reponse`
  MODIFY `REP_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `iota_user`
--
ALTER TABLE `iota_user`
  MODIFY `USER_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT pour la table `iota_workspace`
--
ALTER TABLE `iota_workspace`
  MODIFY `WORK_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
