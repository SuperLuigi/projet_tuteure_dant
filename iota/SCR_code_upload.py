from flask import render_template, redirect, url_for
from iota.db import connexion
from scripts.SCR_correction import *

def code_upload(request, action):
	conn = connexion()
	cursor = conn.cursor()
	sql=''
	
	if action == 'add':
		nom = request.form['nom']
		description = request.form['description']
		nomfunc = request.form['nomfunc']
		extension = request.form['code_languages']
		timeout = abs(int(request.form['timeout']))
		funcbody = request.form['funcbody']
		id_work = request.form['id_work']

		path_to_file = "uploaded_code/"+nomfunc+"."+extension+""
		
		f= open(path_to_file, "w+")
		f.write(funcbody)
		f.close()

		est_trouve = check_func_name(nomfunc, path_to_file)
		
		#data exo
		sql_data_exo = "SELECT * FROM iota_exo INNER JOIN iota_ref_exo_workspace ON EXO_id = ref_EXO_id INNER JOIN iota_workspace ON WORK_id = ref_WORK_id WHERE WORK_id = "+str(id_work)+" "
		cursor.execute(sql_data_exo)
		data = cursor.fetchall()
		#data workspace
		sql_data_work = "SELECT * FROM iota_workspace WHERE WORK_id = "+str(id_work)+" "
		cursor.execute(sql_data_work)
		data_work = cursor.fetchone()
		
		try:
			if est_trouve:
				function_arguments = find_func_args(path_to_file, nomfunc, extension)
				if function_arguments[0] and function_arguments[3] and function_arguments[2] is not None:
					fonction_vide = function_parser(nomfunc, path_to_file, extension)
					if fonction_vide is not None :
						fonction_vide = ''.join([str(x) for x in fonction_vide])
						sql = "INSERT INTO iota_exo SET EXO_nom = '"+nomfunc+"', EXO_content = '"+fonction_vide+"', EXO_lang='"+extension+"',  EXO_path='"+path_to_file+"', EXO_timeout=" + str(timeout) + " "
					else:
						err = "Erreur lors de l'envoie de votre code"
						return render_template('enseignant/workspace.html', data=data, data_work=data_work, error=err)
				else:
					err = "Les arguments n'ont pas pu être trouvés"
					return render_template('enseignant/workspace.html', data=data, data_work=data_work, error=err)
			else:
				err = "Il y a une différence entre le nom de la fonction spécifié et le nom de la fonction dans le code"
				return render_template('enseignant/workspace.html', data=data, data_work=data_work, error=err)
		except:
			err = "Soit la fonction est mal typée, soit il y a un problème avec le type de retour, soit avec le nom de la fonction"
			return render_template('enseignant/workspace.html', data=data, data_work=data_work, error=err)

	if action == 'supp':
		id = request.form['nom']
		
		sql = "DELETE FROM iota_exo WHERE EXO_id = '"+id+"' "
	
	cursor.execute(sql)
	last_id = cursor.lastrowid
	
	sql2 = "INSERT INTO iota_ref_exo_workspace SET ref_EXO_id = "+str(last_id)+", ref_WORK_id = "+str(id_work)+" "
	cursor.execute(sql2)
	
	conn.commit()
	#return 'upload'
	return redirect(url_for('ens_workspace', id=id_work))
