from flask import render_template, redirect, url_for, session
from iota.db import connexion
from scripts.SCR_correction import *


def gest_reponse(request, action):
	conn = connexion()
	cursor = conn.cursor()
	sql=''
	
	if action == 'add':
		funcbody = request.form['funcbody']
		id_exo = request.form['id_exo']
		id_user = session['user'][0]
		nomfonc = request.form['nomfonc']
		extension = request.form['code_languages']
		timeout = abs(int(request.form['timeout']))
		exo_path = request.form['exo_path']
		
		path_to_file = "reponse_code/"+nomfonc+"."+extension+""
		
		f= open(path_to_file, "w+")
		f.write(funcbody)
		f.close()
		
		sql = "INSERT INTO iota_reponse SET REP_content = '"+funcbody+"', REP_USER_id = "+str(id_user)+", REP_EXO_id = "+id_exo+" "
		
		tab_result = compare_n_times(5, nomfonc, exo_path, path_to_file, extension, timeout)
		score = 0
		for row in tab_result:
			if row:
				score=score+1
		score = abs(score*20)
		if score > 100:
			score = 100
		return render_template('etudiant/result.html', score=score)
		#return redirect(url_for('etu_result', score=score))
		
		
	if action == 'supp':
		id = request.form['nom']
		
		sql = "DELETE FROM iota_reponse WHERE REP_id = '"+id+"' "
	
	cursor.execute(sql)
	conn.commit()
	#return 'upload'
	return redirect(url_for('etu_workspace'))

#
# def test():
# 	student_score = compare_n_times(3, "square", "code_submission/square_prof.py", "code_submission/square_student.py", "py")
# 	print(student_score)
#
#
# if __name__ == '__main__':
# 	test()
