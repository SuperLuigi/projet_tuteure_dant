from flask import render_template, redirect, url_for, session
from iota.db import connexion

def user_connect(request):
	conn = connexion()
	cursor = conn.cursor()
	sql=''
	mail = request.form['mail']
	mdp = request.form['mdp']
	
	sql=("SELECT * FROM iota_user WHERE USER_mail = '"+mail+"' "
            "AND USER_password = '"+mdp+"' ")
	cursor.execute(sql)
	result = cursor.fetchone()
	if result:
		session['user'] = result
		if result[6] == 'enseignant':
			sql2="SELECT * FROM iota_complement_enseignant WHERE ens_USER_id = "+str(result[0])+" "
			cursor.execute(sql2)
			result_ens = cursor.fetchall()
			session['user_complement'] = result_ens
			return redirect(url_for('ens_dashboard'))
		else:
			return redirect(url_for('etu_dashboard'))
	else:
		return redirect(url_for('index', err="Identifiants incorrectes"))
	
	#return sql
	return redirect(url_for('etu_dashboard'))

def user_connect_id(id):
	conn = connexion()
	cursor = conn.cursor()
	sql=''
	
	sql="SELECT * FROM iota_user WHERE USER_id = "+str(id)+" "
	cursor.execute(sql)
	result = cursor.fetchone()
	session['user'] = result 
	
	if result[6] == 'enseignant':
		sql2="SELECT * FROM iota_complement_enseignant WHERE ens_USER_id = "+str(id)+" "
		cursor.execute(sql2)
		result_ens = cursor.fetchall()
		session['user_complement'] = result_ens
		return redirect(url_for('ens_dashboard'))
	
	#return sql
	return redirect(url_for('etu_dashboard'))

def user_disconnect():
	session.pop('user', None)
	return redirect(url_for('index'))