from flask import Flask, escape, url_for, render_template
from flaskext.mysql import MySQL
'''from . import db'''

app = Flask(__name__)
app.secret_key=b'_5#y2L"F4Q8z\n\xec]/'

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'bdd_tuteure'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

conn = mysql.connect()
cursor =conn.cursor()

cursor.execute("SELECT * from iota_user")
data = cursor.fetchone()

import iota.route

with app.test_request_context():
    print(url_for('index'))
    print(url_for('login'))
    print(url_for('login', next='/'))

'''db.init_app(app)'''