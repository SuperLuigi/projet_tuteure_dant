#include <iostream>
using namespace std;

extern "C"{
    void myFunction() {
        cout << "I just got executed!\n" << endl;
        // return "I just got executed";
        // return 1;
    }

    int main(){
        cout << "MAIN !" << endl;
        return 1;
    }   
}
