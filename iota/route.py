import math

from flask import Flask, escape, url_for, render_template, request, session, redirect
from iota import app
from iota.SCR_code_upload import code_upload
from iota.SCR_workspace import gest_workspace
from iota.SCR_reponse import gest_reponse
from iota.SCR_inscription import gest_inscription
from iota.db import connexion
from iota.SCR_connexion import user_connect, user_disconnect
from iota.SCR_workspace import rejoindre_workspace

@app.route('/')
def index():
	err = ''
	if request.args.get('err'):
		err = request.args.get('err')
	return render_template('index.html', err=err)

@app.route('/login')
def login():
	return 'login'

#route enseignant
@app.route('/enseignant/dashboard')
def ens_dashboard(data=None):
	if 'user' in session:
		if 'enseignant' in session['user']:
			conn = connexion()
			cursor = conn.cursor()
			sql = "SELECT * FROM iota_workspace WHERE WORK_USER_id = "+str(session["user"][0])+""
			cursor.execute(sql)
			data = cursor.fetchall()
			return render_template('enseignant/dashboard.html', data=data)
		return	'Vous devez etre enseignant'
	return 'Not logged in'
	
@app.route('/enseignant/workspace')
def ens_workspace(data=None):
	if 'user' in session:
		if 'enseignant' in session['user']:
			conn = connexion()
			cursor = conn.cursor()
			id_work = request.args.get('id')
			#data exo
			sql = "SELECT * FROM iota_exo INNER JOIN iota_ref_exo_workspace ON EXO_id = ref_EXO_id INNER JOIN iota_workspace ON WORK_id = ref_WORK_id WHERE WORK_id = "+str(id_work)+" "
			cursor.execute(sql)
			data = cursor.fetchall()
			#data workspace
			sql = "SELECT * FROM iota_workspace WHERE WORK_id = "+str(id_work)+" "
			cursor.execute(sql)
			data_work = cursor.fetchone()
			return render_template('enseignant/workspace.html', data=data, data_work=data_work)
		return	'Vous devez etre enseignant'
	return 'Not logged in'

#route etudiant
@app.route('/etudiant/dashboard')
def etu_dashboard():
	if 'user' in session:
		if 'eleve' in session['user']:
			conn = connexion()
			cursor = conn.cursor()
			sql = "SELECT * FROM iota_workspace INNER JOIN iota_ref_user_workspace ON WORK_id = ref_WORK_id INNER JOIN iota_user ON USER_id = ref_USER_id WHERE ref_USER_id = "+str(session['user'][0])+" "
			cursor.execute(sql)
			data = cursor.fetchall()
			return render_template('etudiant/dashboard-etu.html', data=data)
		return	'Vous devez etre eleve'
	return 'Not logged in'

@app.route('/etudiant/workspace')
def etu_workspace():
	if 'user' in session:
		if 'eleve' in session['user']:
			conn = connexion()
			cursor = conn.cursor()
			id_ws = request.args.get('id_ws')
			sql = "SELECT EXO_id, EXO_nom FROM iota_exo INNER JOIN iota_ref_exo_workspace ON EXO_id = ref_EXO_id INNER JOIN iota_workspace ON WORK_id = ref_WORK_id WHERE WORK_id=" + str(id_ws) + ""
			cursor.execute(sql)
			data = cursor.fetchall()
			sql2 = "SELECT * FROM iota_workspace WHERE WORK_id=" + str(id_ws) + ""
			cursor.execute(sql2)
			data2 = cursor.fetchone()
			return render_template('etudiant/workspace-etu.html', data=data, data2=data2)
		return	'Vous devez etre eleve'
	return 'Not logged in'

@app.route('/etudiant/reponse')
def etu_reponse():
	if 'user' in session:
		if 'eleve' in session['user']:
			conn = connexion()
			cursor = conn.cursor()
			id_exo = request.args.get('id')
			sql = "SELECT * FROM iota_exo WHERE EXO_id="+id_exo+" "
			cursor.execute(sql)
			data = cursor.fetchone()
			return render_template('etudiant/reponse.html', data=data)
		return	'Vous devez etre eleve'
	return 'Not logged in'

@app.route('/etudiant/result')
def etu_result():
	if 'user' in session:
		if 'eleve' in session['user']:
			score_calc = request.args.get('score')
			score_calc = abs(int(float((score_calc)))*20)
			if score_calc > 100:
				score_calc = 100
			return render_template('etudiant/result.html', score=score_calc)
		return	'Vous devez etre eleve'
	return 'Not logged in'

#route inscription
@app.route('/inscription/enseignant')
def ens_inscription():
	return render_template('inscription/inscription-enseignant.html')

@app.route('/inscription/etudiant')
def etu_insctiprion():
	return render_template('inscription/inscription-etudiant.html')

#script route
@app.route('/script/code_upload', methods = ['GET', 'POST'])
def scr_code_upload():
	if request.method == 'GET':
		return 'upload_get'
	if request.method == 'POST':
		action = request.args.get('action')
		return code_upload(request, action)
	return 'upload'
	
@app.route('/script/gest_workspace', methods = ['GET', 'POST'])
def scr_gest_workspace():
	if request.method == 'GET':
		return 'upload_get'
	if request.method == 'POST':
		action = request.args.get('action')
		return gest_workspace(request, action)
	return 'upload'

@app.route('/script/gest_reponse', methods = ['GET', 'POST'])
def scr_gest_reponse():
	if request.method == 'GET':
		return 'upload_get'
	if request.method == 'POST':
		action = request.args.get('action')
		return gest_reponse(request, action)
	return 'upload'

@app.route('/script/gest_inscription', methods = ['GET', 'POST'])
def scr_gest_inscription():
	if request.method == 'GET':
		return 'upload_get'
	if request.method == 'POST':
		action = request.args.get('action')
		return gest_inscription(request, action)
	return 'upload'

@app.route('/script/connexion', methods = ['GET', 'POST'])
def scr_connexion():
	return user_connect(request)

@app.route('/script/deconnexion', methods = ['GET', 'POST'])
def scr_deconnexion():
	return user_disconnect()


@app.route('/script/add_ws', methods=['GET', 'POST'])
def scr_add_ws():
	if request.method == 'POST':
		codews = request.form['code']
		return rejoindre_workspace(codews)
