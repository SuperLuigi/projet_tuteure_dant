from flask import render_template, redirect, url_for, session
from iota.db import connexion
from scripts.SCR_correction import *

def gest_workspace(request, action):
	conn = connexion()
	cursor = conn.cursor()
	sql=''
	
	if action == 'add':
		nom = request.form['nom']
		description = request.form['description']
		work_code = generer_code()
		
		sql = "INSERT INTO iota_workspace SET WORK_nom = '"+nom+"', WORK_desc = '"+description+"', WORK_code = "+str(work_code)+", WORK_USER_id = "+str(session["user"][0])+""
		
	if action == 'supp':
		id = request.form['nom']
		
		sql = "DELETE FROM iota_workspace WHERE WORK_id = '"+id+"' "
	
	cursor.execute(sql)
	conn.commit()
	#return 'upload'
	return redirect(url_for('ens_dashboard'))


def rejoindre_workspace(code_ws):
	conn = connexion()
	cursor = conn.cursor()
	sql = ''

	sql = "SELECT * FROM iota_workspace WHERE WORK_code = " + str(code_ws) + ""
	cursor.execute(sql)
	tab = cursor.fetchone()

	if tab:
		sql2 = "INSERT INTO iota_ref_user_workspace SET ref_USER_id=" + str(session["user"][0]) + ", ref_WORK_id=" + str(tab[0]) + " "
		cursor.execute(sql2)
	
	conn.commit()
	return redirect(url_for('etu_dashboard'))
