import os
import random
import string
from difflib import SequenceMatcher  # detection du plagiat si on a le temps
import subprocess  # pour appels systeme
from typing import List
import tempfile

#import pandas as pd
#import numpy as np
#import matplotlib.pyplot as plt  # pour afficher graphiques d'information au professeur (histogrammes des notes, etc.)
# import signal
#from ctypes import *  # pour récupérer la soumission de l'élève et la correction du prof
#from numpy.ctypeslib import ndpointer  #
import time  # pour calculer durée exécution soumission de l'élve et correction du prof


def file_keywords_verification(fichier):
    syscall = ["<unistd.h>",
               "<sys/syscall.h>",
               "<errno.h>",
               "fork",
               "wait",
               "waitpid",
               "wait4",
               "exec",
               "scanf",
               "scan",
               "print"
               "printf"
               "fputs",
               "fgets",
               "fget",
               "get",
               "exit",
               "exec",
               "execl",
               "execlp",
               "execlpe",
               "execv",
               "stdout"]

    # si le fichier est vide
    if not fichier:
        return False

    # si le fichier contient un mot interdit
    for sc in syscall:
        if sc in fichier:
            return False

    # il faut gérer le cas de gets
    for i in range(5, len(fichier)):
        if (fichier[i - 3] + fichier[i - 2] + fichier[i - 1] + fichier[i]) == "gets":
            if fichier[i - 4] != "f":
                return False
    return True


def load_file(path_of_file, file_name, extension):
    with open(path_of_file + file_name + '.' + extension, 'r') as file:
        data = file.read().replace("\n", "")
    data.strip("    ")  # remove the tab
    data = ''.join(data.split())

    return data


# liste les types pour un language
def types_list(language: str) -> list:
    if language == "c":
        return ["int", "double", "float", "char", "short", "long"]
    elif language == "cpp":  # + string, bool
        return ["int", "double", "float", "bool", "string", "char", "short", "long"]
    else:
        return ["int", "float", "bool", "str"]


# automatise l'import du code soumis par le prof, retourne le code sous forme de texte
def function_loader(path_to_file: str) -> list:
    with open(path_to_file) as file_in:
        lines = []
        for line in file_in:
            if line != "\n":
                lines.append(line)
    return lines


# réservé à l'étudiant (enlève le corps de la fonction codé par le prof)
def function_parser(prof_function_name: str, path_to_file: str, extension: str) -> List:
    """
    parse the teacher function to the student and empty the body of the function
    :param prof_function_name: name of teacher function
    :param path_to_file: path to the file, ex: code_submission_square_prof.py
    :param extension: c, cpp ou py
    :return: the function without the answer
    """
    lines = function_loader(path_to_file)
    string_to_student = []
    function_name_set = False

    if extension == "c" or extension == "cpp":
        parenthesis_count = 0

        if len(lines) < 3:
            temp = []
            for i in range(len(lines)):
                temp_array = lines[i].split("{")[0]
                if "#include" not in temp_array:
                    temp_array += "{\n"
                temp.append(temp_array)

            lines = temp
            lines.append("    // à remplir")
            lines.append("\n}")
            string_to_student = lines

        else:
            for i in range(len(lines)):
                parenthesis_count += lines[i].count("{")
                parenthesis_count -= lines[i].count("}")

                if "#include" in lines[i]:
                    string_to_student.append(lines[i])

                if parenthesis_count != 0 and not function_name_set:
                    string_to_student.append(lines[i])
                    string_to_student.append("// à remplir")
                    function_name_set = True

                if prof_function_name in lines[i]:
                    pass
                if "}" in lines[i]:
                    if parenthesis_count == 0:
                        string_to_student.append("\n}")

    else:
        for i in range(len(lines)):
            if ("def" and prof_function_name) in lines[i]:
                string_to_student.append(lines[i])
                function_name_set = True
                if function_name_set:
                    break
            else:
                return None
        string_to_student.append("  pass")

    return string_to_student


def check_func_name(func_name: str, path_to_file: str) -> bool:
    """
    check that the function's name has the correct name
    :param func_name:
    :param path_to_file:
    :return: si le nom de la fonction est correcte
    """
    if func_name is None or func_name == "" or " " in func_name:
        return False

    lines = function_loader(path_to_file)

    for i in range(len(lines)):
        if func_name in lines[i]:
            if func_name in lines[i].split(" ")[1]:
                return True
    return False


def find_func_args(path_of_file: str, name_of_func: str, extension: str) -> (bool, str or None, list or None, bool):
    """
    - Trouve les arguments dans la fonction
    - Vérifie que les arguments de la fonction et et le type de retour sont bien corrects
    :returns
        1. bool: si le nom de la fonction a été trouvé ou pas
        2. str: le nom de la fonction (à vérifier si c'est bien le même)
        3. list: les paramètes d'entrée + leur types 
        4. list: si la fonction est correctement typée
    """

    file = function_loader(path_of_file)

    index_func_name = -1
    for i in range(len(file)):
        if name_of_func in file[i]:
            if name_of_func in file[i].split(" ")[1]:
                index_func_name = i
                break
            else:
                return False, None, None, False
    if index_func_name == -1:  # if the name of the function has not been found
        return False, None, None, False

    if extension == "c" or extension == "cpp":
        # obtaining the return type (not for python since its at the end if specified)
        return_type: str = file[index_func_name].split(" "+name_of_func+"(")[0]

        # retrieve the input arguments
        input_args = file[index_func_name].split(" "+name_of_func+"(")[1]
        input_args = input_args.split(")")[0]
        input_args = input_args.split(",")
        input_args = [x.strip(' ') for x in input_args]

        # check type is specified:
        correctly_typed = True
        types = types_list(extension)
        for i in range(len(input_args)):
            type_left_var_right = input_args[i].split(' ')
            if len(type_left_var_right) < 2 or len(type_left_var_right) > 3:
                correctly_typed = False
                break

            if len(type_left_var_right) == 2:
                if "[]" in type_left_var_right[1]:  # vérifie que correct même si tableau
                    type_left_var_right[1] = type_left_var_right[1].split("[]")[0]

                if type_left_var_right[0] not in types:
                    correctly_typed = False
                    break
            else:
                if "[]" in type_left_var_right[2]:  # vérifie que correct même si tableau
                    type_left_var_right[2] = type_left_var_right[2].split("[]")[0]

                if type_left_var_right[1] not in types:
                    correctly_typed = False
                    break
                if extension == "c" or "cpp":
                    if type_left_var_right[0] != "signed" and type_left_var_right[0] != "unsigned":
                        correctly_typed = False
                        break
        return True, return_type, input_args, correctly_typed

    else:
        # obtaining the return type
        return_type: str = file[index_func_name].split("->")[1]
        return_type = return_type.split(":")[0]  # remove ":"

        # retrieve the input arguments
        input_args = file[index_func_name].split(" "+name_of_func+"(")[1]
        input_args = input_args.split(")")[0]
        input_args = input_args.split(",")
        input_args = [x.strip(' ') for x in input_args]

        correctly_typed = True
        types = types_list(extension)
        for i in range(len(input_args)):
            type_right_var_left = input_args[i].split(': ')
            if len(type_right_var_left) != 2:
                correctly_typed = False
                break
            if type_right_var_left[1] not in types:
                correctly_typed = False
                break
        return True, return_type, input_args, correctly_typed


def automatic_args_generation(input_arguments: List[str], extension: str) -> List[str]:
    """
    Génère des valeurs aléatoires à partir des arguments de la fonction
    :param input_arguments: la liste des arguments avec leur type, ex : ['unsigned int i', 'signed int k', 'char c']
    :param extension: l'extension du fichier (.c, .cpp, ou .py)
    :return: retourne la liste contenant les aleurs aléatoires pour chaque arguments, ex: [130, -39.168247157947, 'Y']
    """

    random_values: List = []
    for i in range(len(input_arguments)):
        is_signed = None
        if extension == "c" or extension == "cpp":
            var_type = input_arguments[i].split(" ")
            if len(var_type) > 2:
                is_signed = var_type[0]
                var_type = var_type[1]
            else:
                var_type = var_type[0]
        else:
            var_type = input_arguments[i].split(": ")
            var_type = var_type[1]

        if var_type == "int" or var_type == "long" or var_type == "short":
            random_values.append(random.randint(-300, 300))
        elif var_type == "double" or var_type == "float":
            random_values.append(random.uniform(-100.0, 100.0))
        elif var_type == "char":
            character = random.choice(string.ascii_letters)
            random_values.append(character)
        elif var_type == "string" or var_type == "str":
            random_values.append(''.join(random.choice(string.ascii_letters) for m in range(50)))
        else:  # if bool
            if extension == "py":
                random_values.append(random.choice([True, False]))
            else:
                random_values.append(random.choice(["true", "false"]))

        if is_signed == "unsigned":
            random_values[i] = abs(random_values[i])

    return random_values


def test_the_function(file_path_no_extension: str, passed_rand_values: List, function_type: str,
                      extension: str, name_of_function: str, choice_timeout: int) -> List or None:
    """
    Fonction qui teste la soumission en entrée. A exécuter 2 fois: 1 fois la fonction de l'élève, 1 fois celle du prof
    :param file_path_no_extension: chemin vers le fichier, sans l'extension
    :param passed_rand_values: les paramètres avec les valeurs aléatoires
    :param function_type: int, bool , etc
    :param extension: c, cpp, py
    :param name_of_function: the name of the function
    :param choice_timeout: the timeout we want
    :return: liste contenant le résultat, ou None si erreur dans le programme
    """

    # si on reçoit un float
    if isinstance(choice_timeout, str):
        choice_timeout = float(choice_timeout)

    choice_timeout = abs(int(choice_timeout))

    code_to_compare = []
    if extension == "c" or extension == "cpp":
        code_to_compare = ["#include <stdio.h>", "#include <stdlib.h>\n"]

    # génère le code depuis la fonction du prof:
    code_to_compare += function_loader(file_path_no_extension+"."+extension)

    if extension == "c" or extension == "cpp":
        if function_type == "int":
            variable_access = "d"
        elif function_type == "long":
            variable_access = "ld"
        elif function_type == "float" or "double":
            variable_access = "f"
        else:
            variable_access = "bool"

        code_to_compare.append("\nint main(int argc, char *argv[]){\n    printf(\"%"+variable_access+"\", "+name_of_function+"(")
        for i in range(len(passed_rand_values)):
            if isinstance(passed_rand_values[i], str):
                code_to_compare[-1] += "\'"+passed_rand_values[i]+"\'"
            else:
                code_to_compare[-1] += str(passed_rand_values[i])
            if i < len(passed_rand_values)-1:
                code_to_compare[-1] += ", "
        code_to_compare[-1] += "));\n    return 0;\n}"

        # sauvegarde le code dans un fichier c
        with open(file_path_no_extension + "_test."+extension, 'w') as f:
            for item in code_to_compare:
                f.write("%s\n" % item)

        # compile le fichier
        if extension == "c":
            errors = subprocess.call(["gcc", file_path_no_extension + "_test.c", "-o",
                                      file_path_no_extension+"_test"], stderr=True)
            if errors != 0:  # vérifie que pas d'erreur de compilation
                return None
        elif extension == "cpp":
            errors = subprocess.call(["g++", file_path_no_extension + "_test.cpp", "-o",
                                      file_path_no_extension+"_test"], stderr=True)
            if errors != 0:  # vérifie que pas d'erreur de compilation
                return None

        value_tested = []
        try:
            with tempfile.TemporaryFile() as tempf:
                proc = subprocess.call([file_path_no_extension+"_test"], stdout=tempf, timeout=choice_timeout)
                tempf.seek(0)
                value_tested.append(tempf.read())
            return value_tested
        except subprocess.TimeoutExpired:
            return None

    else:  # fichier python

        # enlève les types
        types = types_list("py")
        for i in range(len(code_to_compare)):
            for j in range(len(types)):
                code_to_compare[i] = code_to_compare[i].replace(": "+types[j], "")
                code_to_compare[i] = code_to_compare[i].replace(types[j], "")
                code_to_compare[i] = code_to_compare[i].replace("-> "+types[j], "")

            code_to_compare[i] = code_to_compare[i].replace("-> ", "")
            code_to_compare[i] = code_to_compare[i].replace(" :", ":")

        # génère le code à exécuter
        code_to_compare.append("\nif __name__ == \"__main__\":\n    print(" + name_of_function + "(")
        for i in range(len(passed_rand_values)):
            if isinstance(passed_rand_values[i], str):
                code_to_compare[-1] += "\""+passed_rand_values[i]+"\""
            else:
                code_to_compare[-1] += str(passed_rand_values[i])
            if i < len(passed_rand_values) - 1:
                code_to_compare[-1] += ", "
        code_to_compare[-1] += "))"

        with open(file_path_no_extension + "_test."+extension, 'w') as f:
            for item in code_to_compare:
                f.write("%s\n" % item)

        try:
            error = subprocess.call(["python", file_path_no_extension + "_test.py"], stderr=True, timeout=choice_timeout)
            if error != 0:
                return None
        except subprocess.TimeoutExpired:
            return None

        value_tested = []
        try:
            with tempfile.TemporaryFile() as tempf:
                proc = subprocess.Popen(['python', file_path_no_extension+"_test.py"], stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT)

                value_tested.append((proc.communicate()[0]))
            return value_tested
        except subprocess.TimeoutExpired:
            return None


# réservé à l'élève
def compare_n_times(nb_verification: int, name_of_func: str, path_to_teacher_func: str,
                    path_to_student_func: str, extension: str, choice_timeout: int) -> List[bool]:
    """
    Sert à vérifier n fois la soumission de l'élève
    :param nb_verification: le nombre de vérifications voulues
    :param name_of_func: le nom de la fonction
    :param path_to_teacher_func: le chemin vers le fichier du professeur (avec l'extension)
    :param path_to_student_func: la même mais pour l'élève
    :param extension: c, cpp ou py
    :return: List[bool] valant True si la soumission de l'élève est la même que le prof, False sinon
    """

    verification_results = []
    if extension == "c":
        upper_bound = 2
    elif extension == "cpp":
        upper_bound = 4
    else:
        upper_bound = 3

    for i in range(nb_verification):
        # test avec le prof
        func_found, func_type, args_to_test, are_types_correct = find_func_args(path_to_teacher_func, name_of_func, extension)
        passed_values = automatic_args_generation(args_to_test, extension)    # values used to compare student and teacher
        prof_value_from_test = test_the_function(path_to_teacher_func[:-upper_bound], passed_values, func_type, extension, name_of_func, choice_timeout)
        print("prof_value_from_test: ", prof_value_from_test)

        # test avec l'étudiant
        func_found, func_type, args_to_test, are_types_correct = find_func_args(path_to_student_func, name_of_func, extension)
        etud_value_from_test = test_the_function(path_to_student_func[:-upper_bound], passed_values,  func_type, extension, name_of_func, choice_timeout)
        print("etud_value_from_test: ", etud_value_from_test)

        if prof_value_from_test == etud_value_from_test:
            verification_results.append(True)
        else:
            verification_results.append(False)

    return verification_results


# génération du code pour l'enseignant
def generer_code() -> int:
    return random.randint(10000000, 99999999)



########################################################################################################################
########################################################################################################################
########################################################################################################################
if __name__ == '__main__':
    # test()
    file_path = "../iota/code_submission/square_prof.c"

    function_name = "square"

    parsed_function = function_parser(function_name, file_path, "c")  # parse the teacher function
    # print("parsed_function: ", parsed_function)
    # print(check_func_name("square", file_path))  # check the name of the function is present
    # print(find_func_args("code_submission/square_prof.c", "square", "c"))
    # the_args = find_func_args(file_path, "square", "py")[2]
    # the_func_type = find_func_args(file_path, "square", "py")[1]
    # rand_values = automatic_args_generation(the_args, "py")
    # test_the_function(file_path[:-3], rand_values, the_func_type, "py", function_name)
    print(parsed_function)

    #                                       DÉCOMMENTER POUR TESTER
    # test avec C
    # student_score = compare_n_times(3, function_name, "./code_submission/square_prof.c", "./code_submission/square_student.c", "c", 3)
    # print(student_score)

    # test avec C++
    # student_score = compare_n_times(3, function_name, "./code_submission/square_prof.cpp", "./code_submission/square_student.cpp", "cpp", 3)
    # print(student_score)

    # test avec Python
    # student_score = compare_n_times(3, function_name, "../iota/code_submission/square_prof.py", "../iota/code_submission/square_student.py", "py", 3)
    # print(student_score)

    # generer_code()












