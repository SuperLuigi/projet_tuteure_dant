import os
import random
import string
from difflib import SequenceMatcher  # detection du plagiat si on a le temps
import subprocess  # pour appels systeme
from typing import List
import tempfile

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt  # pour afficher graphiques d'information au professeur (histogrammes des notes, etc.)
# import signal
from ctypes import *  # pour récupérer la soumission de l'élève et la correction du prof
from numpy.ctypeslib import ndpointer  #
import time  # pour calculer durée exécution soumission de l'élve et correction du prof


# pour plagiat plus tard si on a le temps
# exemple : print(similarity_rate(data1, data2))
# def similarity_rate(a, b):
#     return SequenceMatcher(None, a, b).ratio()


# montre comment un fichier de loop infinie s'arrete au bout d'un moment (ici, 3 sec)
def executer_fichier_loop_infinie():
    try:
        subprocess.run(["gcc", "-o", "./code_submission/loop", "./code_submission/loop.c"],
                       check=True)  # compile le fichier, check permet de gerer les erreurs de compilation
        subprocess.run(["./code_submission/loop"], check=True, timeout=3)
    except FileNotFoundError:
        print("Fichier introuvable")
    except TimeoutError:
        print("Temps dépassé")



def load_file(path_of_file, file_name, extension):
    with open(path_of_file + file_name + '.' + extension, 'r') as file:
        data = file.read().replace("\n", "")
    data.strip("    ")  # remove the tab
    data = ''.join(data.split())

    return data


def test():
    data1 = load_file('../iota/code_submission/', 'test', 'c')
    data2 = load_file('../iota/code_submission/', 'test2', 'c')
    data3 = load_file('../iota/code_submission/', 'loop', 'c')
    # print("data1 = ", data1)
    # print("data2 = ", data2)
    # Test si correct
    # print("fichier 1 correct ? ", file_keywords_verification(data1))
    # print("fichier 2 correct ? ", file_keywords_verification(data2))
    # print("fichier 3 correct ? ", file_keywords_verification(data3))
    # executer_fichier_loop_infinie()   # exécute la boucle infinie et montre quelle est stoppée
    """
        # compile le fichier et l'exécute exo1
        if file_keywords_verification(data1):
            try:
                subprocess.run(["gcc", "-o", "./code_submission/test", "./code_submission/test.c"], check=True, timeout=5)     # compile le fichier, check permet de gérer les erreurs de compilation
                subprocess.run(["./code_submission/test"], check=True, timeout=5)
            except FileNotFoundError:
                print("Fichier introuvable")
                # ici on définira le score à 0/10
        """
    # ----- c ------ #
    # pour tester: doit compiler le code c du prof de cette maniere:
    # gcc -fPIC -Wall -Wextra -shared -o code_submission/square code_submission/square.c
    correction_prof = "./code_submission/square"
    correction = CDLL(correction_prof)
    # print(type(correction))
    # correction.square(10).restype = ndpointer(dtype=c_int, shape=(2,))
    start_time = time.time()
    print(correction.square(10))
    elapsed_time = time.time() - start_time  # 4.1484832763671875e-05
    print("elapsed_time = ", elapsed_time)
    start_time = time.time()
    print(correction.square(3))
    elapsed_time = time.time() - start_time
    print("elapsed_time = ", elapsed_time)
    """
        # test array:
        correction_prof = "./code_submission/array"
        correction = CDLL(correction_prof)
        print(correction.function())
        # correction.function().restype = ndpointer(dtype=c_int, shape=(2,))
        # print(correction.function()) 
        # lib = CDLL('./code_submission/array')
        # lib.function.restype = ndpointer(dtype=c_int, shape=(10,))
        # res = lib.function()
        # print(res)
        """
    # ----- C++ ------
    # pour tester: doit compiler le code c++ du prof de cette maniere:
    # g++ -fPIC -Wall -Wextra  -shared -o code_submission/affichage code_submission/affichage.cpp
    correction_prof = "./code_submission/affichage"
    correction = CDLL(correction_prof)
    # print(type(correction))
    # pour tester: doit compiler le code de l'eleve de cette maniere :
    # g++ -fPIC -shared -o code_submission/affichage_eleve code_submission/affichage_eleve.cpp
    eleve = "./code_submission/affichage_eleve"
    soumission = CDLL(eleve)
    # a = correction.myFunction()
    # a = correction.myFunction()
    # print(correction.myFunction())
    # print("a = |", a, "|")   # montre qu'on ne peut pas recupere l'affichage si c'est un void
    # print("a = ", a)
    # pour array:
    #  g++ -c -fPIC code_submission/function.cpp -o code_submission/function.o
    #  g++ -shared -Wl,-soname,code_submission/library.so -o code_submission/library.so code_submission/function.o
    # lib = CDLL('./code_submission/library.so')
    lib = CDLL('./code_submission/function')
    lib.function.restype = ndpointer(dtype=c_int, shape=(10,))
    res = lib.function()
    print(res)


########################################################################################################################
########################################################################################################################
########################################################################################################################
if __name__ == '__main__':
    # test()
    file_path = "../iota/code_submission/square_prof.c"

    function_name = "square"











