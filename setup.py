from setuptools import setup

setup(
    name='iota',
    packages=['iota'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)