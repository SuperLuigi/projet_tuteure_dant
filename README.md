# README #
![Logo](https://bitbucket.org/SuperLuigi/projet_tuteure_dant/raw/2532fb1c93cf40038304e262c55fd01b135baf18/iota/static/img/iota-blue.png)
ιota est une solution de gestion et correction d'exercices élémentaires de programmation.

### How to install ###

* Lancer Apache et MySQL sur XAMPP
* Ouvrir PHPMyAdmin 
* Créer une nouvelle base de données nommée "bdd_tuteure" et choisir l'encodage "utf8_swedish_ci"
* Copier le contenu du fichier SQL/bdd_tuteure.sql dans l'onglet SQL sur PHPMyAdmin
* Vous aurez besoin de flask, pandas et matplotlib
```bash
pip install flask
pip install flask-mysql
pip install pandas
pip install matplotlib
```

* Aller dans le dossier racine du projet
* Ouvrir un terminal et taper les commandes suivantes (Windows) :
```bash
set FLASK_APP=iota
set FLASK_ENV=development
flask run
```
* Aller sur 127.0.0.1:5000 (flask default)
* **Il est à noter que l'éxecution de code ne fonctionnera pas sous Windows si gcc n'est pas installé sur la machine.**

### Contribution guidelines ###

* [Steve REY](mailto:gtsteve.rey@gmail.com)
* [Massil TAGUEMOUT](mailto:massitaguemout@gmail.com)
* [Pierre PHETSINORATH](mailto:phpierre@hotmail.fr)
* [Robin SAN-VICENTE](mailto:robin.sv@outlook.com)
